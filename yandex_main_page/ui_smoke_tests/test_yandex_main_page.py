"""Smoke тесты на главную страницу Yandex."""

import base64

import pytest
from selenium import webdriver
import time


YANDEX_MAIN_PAGE_URL = "https://yandex.ru/"


@pytest.fixture
def driver_chrome():
    """Фикстура возвращает драйвер для Chrome."""
    driver_chrome = webdriver.Chrome()
    yield driver_chrome
    driver_chrome.quit()


def test_search_by_query(driver_chrome):
    """Тест возможности поиска по запросу.

    1. Переход на страницу "https://yandex.ru/"
    2. Поиск элемента поисковой строки.
    3. Ввод запроса "python".
    4. Выполнение поиска.
    5. Проверка, что результаты поиск не пусты.
    """
    driver_chrome.get(YANDEX_MAIN_PAGE_URL)
    try:
        search_string = driver_chrome.find_element_by_id("text")
        search_string.send_keys("python")
        search_string.submit()
        time.sleep(3)
        result_items = driver_chrome.find_elements_by_class_name("serp-item")
        assert len(result_items) > 0, "Результаты поиска пусты."
    except Exception:
        raise


def test_availability_yandex_passport(driver_chrome):
    """Тест возможности перехода на страницу авторизации в аккаунт по кнопке Войти.

    1. Переход на страницу "https://yandex.ru/"
    2. Поиск элемента кнопки "Войти" для входа в аккаунт Яндекс.
        - поиск по имени класса заголовка.
    3. Нажатие кнопки.
    4. Проверка, что в url новой страницы входит "https://passport.yandex.ru".
    """
    driver_chrome.get(YANDEX_MAIN_PAGE_URL)
    try:
        driver_chrome.find_elements_by_class_name("desk-notif-card__login-new-item-title")[0].click()
        time.sleep(1)
        actual_current_url = driver_chrome.current_url
        expected_url = "https://passport.yandex.ru"
        error_text = f"Ожидаемый url {expected_url} не имеет вхождения в фактическом {actual_current_url}"
        assert "https://passport.yandex.ru" in actual_current_url, error_text
    except Exception:
        raise


def test_virtual_keyboard_close(driver_chrome):
    """Тест возможности перехода на страницу авторизации в аккаунт.

    1. Переход на страницу "https://yandex.ru/"
    2. Открытие виртуальной клавиатуры.
    3. Закрытие виртуальной клавиатуры.
    """
    driver_chrome.get(YANDEX_MAIN_PAGE_URL)
    try:
        driver_chrome.find_element_by_class_name("input__keyboard-button").click()
        time.sleep(1)
        driver_chrome.find_element_by_class_name("keyboard-popup__close").click()
    except Exception:
        raise


def test_check_yandex_home_logo(driver_chrome):
    """Тест сравнивает логотип Яндекс с ожидаемым.

    1. Переход на страницу "https://yandex.ru/"
    2. Получить логотип Яндекс.
    3. Сравнить полученный логотип в base64 с ожидаемым.
    """
    driver_chrome.get(YANDEX_MAIN_PAGE_URL)

    with open("yandex_main_page/test_data/ya_logo.png", "rb") as image_file:
        expected_home_logo_base64 = base64.b64encode(image_file.read()).decode("utf-8")
    try:
        actual_home_logo_base64 = driver_chrome.find_element_by_class_name("home-logo__default").screenshot_as_base64
        err_text = (
            f"Логотип yandex не совпадает с ожидаемым:\n "
            f"Факт.:{actual_home_logo_base64}\n "
            f"Ож.: {expected_home_logo_base64}"
        )
        assert actual_home_logo_base64 == expected_home_logo_base64, err_text
    except Exception:
        raise


def test_availability_base_yandex_services(driver_chrome):
    """Тест возможности перехода на страницы сервисов яндекс.

    В тесте проверяются сервисы: маркет, видео, картинки, новости, карты, переводчик, музыка.
    1. Переход на страницу "https://yandex.ru/"
    2. Поиск элемента сервиса яндекс.
        - поиск по имени класса.
    3. Переход по ссылке на страницу сервиса.
    4. Проверка, что в url новой страницы входит ожидаемый.
    """
    driver_chrome.get(YANDEX_MAIN_PAGE_URL)

    service_list = driver_chrome.find_elements_by_class_name("services-new__item-title")[:7]
    url_list = (
        "https://market.yandex.ru/",
        "https://yandex.ru/video/",
        "https://yandex.ru/images/",
        "https://yandex.ru/news/",
        "https://yandex.ru/maps/",
        "https://translate.yandex.ru/",
        "https://music.yandex.ru/"
    )
    service_per_url_dict = {service: url for service, url in zip(service_list, url_list)}
    try:
        for service, exp_url in service_per_url_dict.items():
            service.click()
            new_window = driver_chrome.window_handles[1]
            driver_chrome.switch_to.window(new_window)
            actual_current_url = driver_chrome.current_url
            driver_chrome.close()
            old_window = driver_chrome.window_handles[0]
            driver_chrome.switch_to.window(old_window)

            err_text = f"Ожидаемый url {exp_url} не входит в фактический {actual_current_url}"
            assert exp_url in actual_current_url, err_text
    except Exception:
        raise
