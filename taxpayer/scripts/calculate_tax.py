"""Скрипт для расчета налога на имущество."""

from taxpayer.taxpayer_tools.property import Apartment, Car, CountryHouse


def check_and_get_value():
    """Проверить  на корректность и получить введеное в консоли значение."""
    while True:
        try:
            value = float(input())
        except ValueError:
            print("Введено неверное значение, повторите ввод...")
        else:
            break
    return value


def main():
    """Расчет налога на имущество."""
    print("Введите количество денег:")
    all_money = check_and_get_value()

    print("Введите стоимость квартиры:")
    apartment = Apartment(check_and_get_value())
    apartment.calculate_tax(all_money)

    print("Введите стоимость машины:")
    car = Car(check_and_get_value())
    car.calculate_tax(all_money)

    print("Введите стоимость дачи:")
    country_house = CountryHouse(check_and_get_value())
    country_house.calculate_tax(all_money)


if __name__ == "__main__":
    main()
