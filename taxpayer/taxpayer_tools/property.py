"""Классы, описывающие имущество налогоплательщика."""


APARTAMENT_TAX = 0.001
CAR_TAX = 0.005
COUNTRYHOUSE_TAX = 0.002


class Property:
    """Базовый класс, описывающий имущество."""
    def __init__(self, worth: float):
        self.worth = worth

    def calculate_tax(self, all_money: float):
        """Расчет налога."""
        raise NotImplementedError("метод calculate_tax не реализован")


class Apartment(Property):
    """Имущество квартира."""
    def __init__(self, worth: float):
        super().__init__(worth)

    def calculate_tax(self, all_money: float):
        """Расчет налога на квартиру."""
        tax = self.worth*APARTAMENT_TAX
        print(f"Налог на квартиру: {tax}")

        if tax > all_money:
            print(f"Для уплаты налога вам не хватает {tax - all_money}")


class Car(Property):
    """Имущество машина."""
    def __init__(self, worth: float):
        """Конструктор класса Car."""
        super().__init__(worth)

    def calculate_tax(self, all_money: float):
        """Расчет налога на машину."""
        tax = self.worth*CAR_TAX
        print(f"Налог на машину: {tax}")

        if tax > all_money:
            print(f"Для уплаты налога вам не хватает {tax - all_money}")


class CountryHouse(Property):
    """Имущество дача."""
    def __init__(self, worth: float):
        """Конструктор класса CountryHouse."""
        super().__init__(worth)

    def calculate_tax(self, all_money: float):
        """Расчет налога на дачу."""
        tax = self.worth*COUNTRYHOUSE_TAX
        print(f"Налог на дачу: {tax}")

        if tax > all_money:
            print(f"Для уплаты налога вам не хватает {tax - all_money}")
