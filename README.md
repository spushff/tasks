# Описание по запуску тестов

### Описание расчета налога на имущество.
taxpayer/taxpayer_tools/property.py - модуль содержит инструменты для работы с налогоплательщиком: базовый и дочерние классы, описывающие имущество налогоплательщика.
taxpayer/scripts/calculate_tax.py - модуль содержит скрипт, который запускает расчет налога на имущество.
После запуска будет предложено ввести необходимые данные и затем предоставляется расчет.
Запуск python taxpayer/scripts/calculate_tax.py

### Описание и запуск smoke ui тестов на главную страницу яндекс.
yandex_main_page/ui_smoke_tests/test_yandex_main_page.py - модуль содержит тесты.
yandex_main_page/test_data/ya_logo.png - файл, используемый в тесте.

Тесты используют webdriver из библиотеки selenium.
Chrome драйвер версии ChromeDriver 92.0.4515.43 

Запуск pytest yandex_main_page/ui_smoke_tests/test_yandex_main_page.py